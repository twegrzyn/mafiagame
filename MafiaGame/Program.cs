﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using MafiaGameEngine.Bootstrap;
using MafiaGameEngine.Controllers;
using MafiaGameEngine.Helpers;
using MafiaGameEngine.Persistence.Repositories;
using MafiaGameEngine.Persistence.SaveLoadGame;
using Microsoft.Extensions.Configuration;

namespace Mafia_Game
{
    internal class Program
    {
        private static Task lifecycleTask;

        private static void Main(string[] args)
        {

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            var showLoadingTask = Task.Run(ShowLoading);

            var config = new Bootstrap().Configure();
            var playerLifecycle = config.Resolve<PlayerLifecycle>();
            var playerController = config.Resolve<PlayerController>();
            var robberController = config.Resolve<RobberyController>();
            var dogRaceController = config.Resolve<IllegalDogRaceController>();
            var fightController = config.Resolve<IllegalFightController>();
            var reputationController = config.Resolve<ReputationController>();

            lifecycleTask = Task.Run(() => playerLifecycle.Run());
            stopWatch.Stop();

            TimeSpan ts = stopWatch.Elapsed;

            string elapsedTime = String.Format("{0:00}.{1:00}",
                 ts.Seconds,
                ts.Milliseconds);

            showLoadingTask.Wait();

            Console.Clear();

            PrintProlog(bool.Parse(config.Resolve<IConfiguration>().GetSection("StartOptions").GetSection("PrologEnabled").Value));
            while (true)
            {
                reputationController.EventProducer += ReputationReached;


                Console.Clear();

                Console.WriteLine("[PRE-ALFA] This is pre-alfa version of this game [PRE-ALFA]");
                Console.WriteLine("ver. " + GetAssemblyFileVersion());
                Console.WriteLine("Runtime: " + elapsedTime);
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Your goal is to achieve 1000 reputation points. ");
                playerController.PrintPlayerDetails();
                Console.WriteLine("Pick what do you want to do: ");
                Console.WriteLine("1. Do robbery.");
                Console.WriteLine("2. Go to dogs race.");
                Console.WriteLine("3. Go to illegal fight.");
                Console.WriteLine("4. Save game.");
                Console.WriteLine("5. Refresh page.");
                Console.WriteLine("0. !!! Restart the game (game will be closed, save files will be removed) !!!");
                var choose = Console.ReadLine();
                try
                {
                    var chooseInt = int.Parse(choose);
                    switch (chooseInt)
                    {
                        case 1:
                            robberController.PrintRobberies();
                            choose = Console.ReadLine();
                            robberController.DoRobbery((RobberyTypeEnum)int.Parse(choose));
                            break;
                        case 2:
                            dogRaceController.GoToRace();
                            Thread.Sleep(2000);
                            break;
                        case 3:
                            fightController.GoToIllegalFight();
                            Thread.Sleep(2000);
                            break;
                        case 4:
                            playerController.SavePlayer();
                            Console.WriteLine("Game saved!");
                            break;
                        case 5:
                            break;
                        case 0:
                            config.Resolve<ISaveLoadGameSerivce>().RemoveSaveFile();
                            Environment.Exit(0);
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine("Invalid action!");
                    Thread.Sleep(2000);
                }

                Thread.Sleep(1000);
            }
        }

        private static void ShowLoading()
        {
            Console.Clear();
            for (var i = 0; i < 101; ++i)
            {
                Console.Write("\rLoading {0}%   ", i);
                Thread.Sleep(1);
            }
            Thread.Sleep(1);
        }

        private static void PrintProlog(bool write)
        {
            if (!write)
                return;
            MachineTextWriter.WriteTextAndConfirm("You are a minor thug, not a real mafia yet, you don't have a car / gun / reputation etc. \n" +
                        "You can only do a few things before you get promoted. \n" +
                        "Try to get promoted in the mafia hierarchy. \n" +
                        "Good luck!");
        }

        public static string GetAssemblyFileVersion()
        {
            Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fileVersion = FileVersionInfo.GetVersionInfo(assembly.Location);
            return fileVersion.FileVersion;
        }

        static void ReputationReached(object sender, EventArgs e)
        {
            Console.Clear();
            Console.WriteLine("[PRE-ALFA] This is pre-alfa version of this game [PRE-ALFA]");
            Console.WriteLine("_____________________________________________________________");
            Console.WriteLine("");
            Console.WriteLine("");
            MachineTextWriter.WriteText("[CONGRATS] YOU WIN THE GAME! [CONGRATS]");
            MachineTextWriter.WriteText("Congratulations. You have achieved 1000 reputation points in the mafia hierarchy. \nYou have been spotted by several mafia families. Soon a messenger will contact you with a proposal. \nFrom now on, you are no longer an ordinary thug. Welcome to the mafia family!");
            MachineTextWriter.WriteText("");
            MachineTextWriter.WriteText("");
            MachineTextWriter.WriteText("Currently, there is no more in this game. ");
            MachineTextWriter.WriteText("Please wait for new updates.");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("_____________________________________________________________");
            MachineTextWriter.WriteText("[PRE-ALFA] This is pre-alfa version of this game [PRE-ALFA]");
            MachineTextWriter.WriteText("ver. " + GetAssemblyFileVersion());
            lifecycleTask.Dispose();
            Thread.Sleep(60000);
            Environment.Exit(0);
        }
    }
}