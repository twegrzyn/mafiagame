﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Configuration;
using MafiaGameEngine.Models.Player;
using MafiaGameEngine.Persistence.Repositories;
using MafiaGameEngine.Persistence.SaveLoadGame;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Module = Autofac.Module;

namespace MafiaGameEngine.Bootstrap
{
    public class Bootstrap : Module
    {
        private readonly ContainerBuilder _builder = new ContainerBuilder();
        private IConfiguration Configuration { get; set; }

        public IContainer Configure()
        {
            var config = new ConfigurationBuilder();
            config.AddJsonFile("config.json");
            Configuration = config.Build();

            var configurationModule = new ConfigurationModule(Configuration);
            _builder.RegisterModule(configurationModule);
            _builder.RegisterInstance(Configuration);


            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("Program", LogLevel.Information)
                    .AddConsole();
            });

            _builder.RegisterInstance(loggerFactory)
                .As<ILoggerFactory>();

            _builder.RegisterGeneric(typeof(Logger<>))
                .As(typeof(ILogger<>))
                .SingleInstance();

            var assembly = Assembly.GetAssembly(typeof(PlayerModel));
            if (assembly == null)
                throw new Exception("Error when loading app!");

            _builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsSelf()
                .SingleInstance()
                .AsImplementedInterfaces();
            RegisterPlayerInstance();
            _builder.RegisterInstance(getSaveLoadPlayerService()).As(typeof(ISaveLoadGameSerivce));

            return _builder.Build();
        }

        private void RegisterPlayerInstance()
        {
            var playerRepository = new PlayerRepository(getSaveLoadPlayerService());
            _builder.RegisterInstance(playerRepository.LoadPlayer()).As<PlayerModel>().SingleInstance().PropertiesAutowired();
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);
        }

        private ISaveLoadGameSerivce getSaveLoadPlayerService()
        {
            if (Configuration.GetSection("Security").GetSection("EncryptSaveFile").Value.ToLower().Equals("true"))
                return new SaveLoadGameFromEncryptedFile();
            else
            {
               return new SaveLoadGameFromFileService();
            }
        }
    }
}