﻿using System.Threading.Tasks;
using Autofac;
using MafiaGameEngine.Helpers;

namespace MafiaGameEngine.Bootstrap
{
    public class MafiaGameEngine
    {
        private static void Main(string[] args)
        {
            var config = new Bootstrap().Configure();
            var playerLifecycle = config.Resolve<PlayerLifecycle>();
            Task.Run(() => playerLifecycle.Run());
            //TODO zrobić to po Bożemu
        }
    }
}
