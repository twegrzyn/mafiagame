﻿using System;
using System.Collections.Generic;
using MafiaGameEngine.Persistence.Repositories;

namespace MafiaGameEngine.Helpers
{
    public static class Randomizer
    {
        private static readonly Random Rand = new Random();

        public static int GetRandomValue(int minvalue, int maxValue)
        {
            return Rand.Next(minvalue, maxValue + 1);
        }

        public static decimal GetRandomValue(decimal minValue, decimal maxValue)
        {
            var randomNumber = (decimal) Rand.NextDouble();
            return minValue + randomNumber * (maxValue - minValue);
        }

        public static long GetRandomValue(long minValue, long maxValue)
        {
            var randomNumber = (long)Rand.NextDouble();
            return minValue + randomNumber * (maxValue - minValue);
        }

        public static string GetRandomDogName()
        {
            List<string> names = DogsNameRepository.GetDogsName();
            return names[Rand.Next(0, names.Capacity)];
        }
    }
}