﻿using Microsoft.Extensions.Configuration;
using System.Timers;
using MafiaGameEngine.Models.Player;
using Microsoft.Extensions.Logging;

namespace MafiaGameEngine.Helpers.Timers
{
    public class HealthTimer
    {
        private readonly Timer _healthTimer = new Timer();
        private readonly PlayerModel _player;
        private readonly int _interval;
        private readonly int _healthRestoreValue;
        private readonly ILogger _logger;

        public HealthTimer(IConfiguration configuration, PlayerModel player, ILogger<HealthTimer> logger)
        {
            _player = player;
            var healthConfig = configuration.GetSection("Health");
            _interval = int.Parse(healthConfig.GetSection("HealthRestoreInterval").Value);
            _healthRestoreValue = int.Parse(healthConfig.GetSection("HealthRestoreValue").Value);
            _logger = logger;
        }

        internal void Prepare()
        {
            _healthTimer.Elapsed += OnTimedEvent;
            _healthTimer.Interval = _interval * 1000;
            _healthTimer.Enabled = true;
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            if (_player.getHealthPointModel().GetHealthPoints() >= _player.getHealthPointModel().GetMaxHealthPoints()) return;

            var startValue = (_player.getHealthPointModel().GetHealthPoints());
            _player.getHealthPointModel().AddHealthPoints(_healthRestoreValue);
            var endValue = _player.getHealthPointModel().GetHealthPoints();

            _logger.LogDebug("Change player health from {0}, to {1}", startValue.ToString(), endValue.ToString());

        }
    }
}
