﻿using System;
using System.Threading;

namespace MafiaGameEngine.Helpers
{
    public class MachineTextWriter
    {
        public static void WriteTextAndConfirm(string text)
        {
            WriteText(text);
            Console.WriteLine("[Press any key to continue]");
            Console.ReadKey();
        }
        public static void WriteText(string text)
        {
            foreach (char c in text)
            {
                Console.Write(c);
                Thread.Sleep(30);
            }
            Console.WriteLine();
        }
    }
}
