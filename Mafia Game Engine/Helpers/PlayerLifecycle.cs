﻿using MafiaGameEngine.Helpers.Timers;

namespace MafiaGameEngine.Helpers
{
    public class PlayerLifecycle
    {
        private readonly HealthTimer _healthTimer;

        public PlayerLifecycle(HealthTimer ht)
        {
            _healthTimer = ht;
        }

        public void Run()
        {
            PrepareTimers();
        }

        private void PrepareTimers()
        {
            _healthTimer.Prepare();
        }
    }
}