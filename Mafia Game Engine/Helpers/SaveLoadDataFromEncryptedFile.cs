﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace MafiaGameEngine.Helpers
{
    public static class SaveLoadDataFromEncryptedFile
    {

        // Encryption setting - HAVE TO BE compiled
        private static readonly string passPhrase = "passphrase11z.,l;sd-1=<Z~!~,cx.a/a";        // can be any string
        private static readonly string saltValue = "''SA/C.XCZ/A/x/a's[q11]]cxz=2058iv(*4m249vczm";        // can be any string
        private static readonly string hashAlgorithm = "SHA1";             // can be "MD5"
        private static readonly int passwordIterations = 3;                  // can be any number
        private static readonly string initVector = "~1B2c3D4e5F6g7H8"; // must be 16 bytes
        private static readonly int keySize = 256;                // can be 192 or 128


        public static void SaveEncryptedData(string data, string path)
        {
            File.WriteAllText(path, Encrypt(data));
        }

        public static string DecryptFile(string path)
        {
            return Decrypt(File.ReadAllText(path));
        }
        private static string Encrypt(string data)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(initVector);
            byte[] rgbSalt = Encoding.ASCII.GetBytes(saltValue);
            byte[] buffer = Encoding.UTF8.GetBytes(data);
            byte[] rgbKey = new PasswordDeriveBytes(passPhrase, rgbSalt, hashAlgorithm, passwordIterations).GetBytes(keySize / 8);
            RijndaelManaged managed = new RijndaelManaged();
            managed.Mode = CipherMode.CBC;
            ICryptoTransform transform = managed.CreateEncryptor(rgbKey, bytes);
            MemoryStream stream = new MemoryStream();
            CryptoStream stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Write);
            stream2.Write(buffer, 0, buffer.Length);
            stream2.FlushFinalBlock();
            byte[] inArray = stream.ToArray();
            stream.Close();
            stream2.Close();
            return Convert.ToBase64String(inArray);
        }

        private static string Decrypt(string data)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(initVector);
            byte[] rgbSalt = Encoding.ASCII.GetBytes(saltValue);
            byte[] buffer = Convert.FromBase64String(data);
            byte[] rgbKey = new PasswordDeriveBytes(passPhrase, rgbSalt, hashAlgorithm, passwordIterations).GetBytes(keySize / 8);
            RijndaelManaged managed = new RijndaelManaged();
            managed.Mode = CipherMode.CBC;
            ICryptoTransform transform = managed.CreateDecryptor(rgbKey, bytes);
            MemoryStream stream = new MemoryStream(buffer);
            CryptoStream stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Read);
            byte[] buffer5 = new byte[buffer.Length];
            int count = stream2.Read(buffer5, 0, buffer5.Length);
            stream.Close();
            stream2.Close();
            return Encoding.UTF8.GetString(buffer5, 0, count);
        }
    }
}
