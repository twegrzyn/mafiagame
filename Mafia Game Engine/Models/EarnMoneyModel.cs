﻿using System;
using MafiaGameEngine.Helpers;

namespace MafiaGameEngine.Models
{
    public class EarnMoneyModel
    {
        private readonly decimal _minEarnMoney;
        private readonly decimal _maxEarnMoney;

        public EarnMoneyModel(decimal minEarnMoney, decimal maxEarnMoney)
        {
            if (minEarnMoney >= maxEarnMoney)
                throw new ArgumentException("Cannot create object with specified values.");
            this._minEarnMoney = minEarnMoney;
            this._maxEarnMoney = maxEarnMoney;
        }


        public decimal GetEarnedMoney()
        {
            return Randomizer.GetRandomValue(_minEarnMoney, _maxEarnMoney);
        }

        public override string ToString()
        {
            return _minEarnMoney + " - " + _maxEarnMoney;
        }
    }
}