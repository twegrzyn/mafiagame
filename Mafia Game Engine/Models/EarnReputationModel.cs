﻿using MafiaGameEngine.Helpers;

namespace MafiaGameEngine.Models
{
    public class EarnReputationModel
    {
        private readonly long _minEarnReputation;
        private readonly long _maxEarnReputation;

        public EarnReputationModel(long minEarnReputation, long maxEarnReputation)
        {
            _minEarnReputation = minEarnReputation;
            _maxEarnReputation = maxEarnReputation;
        }

        public long GetEarnReputation()
        {
            return Randomizer.GetRandomValue(_minEarnReputation, _maxEarnReputation);
        }

        public override string ToString()
        {
            return _minEarnReputation + " - " + _maxEarnReputation;
        }
    }
}