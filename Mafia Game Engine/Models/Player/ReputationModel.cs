﻿namespace MafiaGameEngine.Models.Player
{
    public class ReputationModel
    {
        public ReputationModel()
        {
            _value = 0;
        }

        private decimal _value;

        public decimal GetValue() { return _value; }

        internal void AddReputation(decimal rep)
        {
            _value += rep;
        }

        public override string ToString()
        {
            return _value.ToString("#,##0 Points");
        }
    }
}