﻿namespace MafiaGameEngine.Models.Player
{
    public class MoneyModel
    {
        public MoneyModel()
        {
            _value = 0;
        }

        private decimal _value;

        public decimal GetMoneyValue()
        {
            return _value;
        }

        public void SubstractMoney(decimal money)
        {
            _value -= money;
        }

        public void AddMoney(decimal money)
        {
            _value += money;
        }

        public override string ToString()
        {
            return _value.ToString("#,##0.00 $");
        }
    }
}