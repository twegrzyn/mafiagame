﻿namespace MafiaGameEngine.Models.Player
{
    public class SkillsModel
    {
        public int Strength; // increase attack success percent

        public int Accounting; // decrease % of tax
        // ... //

        public SkillsModel()
        {
            Strength = 0;
            Accounting = 0;
        }
    }
}