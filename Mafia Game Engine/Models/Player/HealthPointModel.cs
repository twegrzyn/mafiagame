﻿namespace MafiaGameEngine.Models.Player
{
    public class HealthPointModel
    {
        public HealthPointModel()
        {
            //Initial value - one time setup
            _healthPoints = 100;
        }

        private int _healthPoints;

        internal int GetHealthPoints()
        {
            return _healthPoints;
        }

        private void SetHealthPoints(int value)
        {
            _healthPoints = value;
        }

        internal void ReduceHeathPoints(int value) => SetHealthPoints(GetHealthPoints() - value);
        internal void SetMaxValueOfHealthPoints(int value) => _maxHealthPoints = value;

        private static int _maxHealthPoints;

        internal int GetMaxHealthPoints()
        {
            return _maxHealthPoints;
        }

        internal void AddHealthPoints(int value) => SetHealthPoints(GetHealthPoints() + value);

        internal void SetHealthPointToMaxValue()
        {
            SetHealthPoints(GetMaxHealthPoints());
        }

        public override string ToString()
        {
            return _healthPoints.ToString("#,##0 Points");
        }
    }
}
