﻿namespace MafiaGameEngine.Models.Player
{
    public class ExperienceModel
    {
        private int _level;
        private long _value;

        public ExperienceModel()
        {
            _value = 0;
            _level = 1;
        }

        internal void AddExperience(long exp)
        {
            _value += exp;
        }

        internal void LevelUp()
        {
            _level++;
        }

        public override string ToString()
        {
            return _value.ToString("#,##0 Points");
        }

        internal int GetLevel()
        {
            return _level;
        }

        internal long GetExperiencePoints()
        {
            return _value;
        }
    }
}