﻿using System.Text;
using Newtonsoft.Json;

namespace MafiaGameEngine.Models.Player
{
    [JsonObject]
    public sealed class PlayerModel
    {
        private readonly MoneyModel _money;
        private readonly ReputationModel _reputation;
        private readonly HealthPointModel _healthPoint;
        private readonly ExperienceModel _experience;
        private readonly SkillsModel _skills;

        public PlayerModel(ExperienceModel exp, HealthPointModel hp, ReputationModel rep, MoneyModel money, SkillsModel skill)
        {
            _money = money;
            _reputation = rep;
            _healthPoint = hp;
            _skills = skill;
            _experience = exp;
        }

        internal MoneyModel getMoneyModel()
        {
            return _money;
        }

        internal ReputationModel getReputationModel()
        {
            return _reputation;
        }

        internal HealthPointModel getHealthPointModel()
        {
            return _healthPoint;
        }

        internal ExperienceModel getExperienceModel()
        {
            return _experience;
        }

        internal SkillsModel getSkillsModel()
        {
            return _skills;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("Level: " + _experience.GetLevel());
            sb.AppendLine("Experience: " + _experience);
            sb.AppendLine("Money: " + _money);
            sb.AppendLine("Reputation: " + _reputation);
            sb.AppendLine("Health point: " + _healthPoint);
            sb.AppendLine();
            return sb.ToString();
        }
    }
}