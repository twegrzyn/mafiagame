﻿namespace MafiaGameEngine.Models
{
    public class RobberyModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int SuccessChance { get; set; } // 0-100
        public int SuperEarnChance { get; set; } // 0-100
        public EarnMoneyModel Money { get; set; }
        public EarnReputationModel Reputation { get; set; }
        public long Experience { get; set; }
        public LostHealthModel LostHealth;

        public override string ToString()
        {
            return Name;
        }
    }
}