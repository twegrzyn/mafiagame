﻿using System;
using MafiaGameEngine.Helpers;

namespace MafiaGameEngine.Models
{
    public class LostHealthModel
    {
        private readonly int _maxLostHealthIfLose;
        private readonly int _maxLostHealthIfWon;

        public LostHealthModel(int maxLostHealthIfLose, int maxLostHealthIfWon)
        {
            _maxLostHealthIfWon = maxLostHealthIfWon;
            _maxLostHealthIfLose = maxLostHealthIfLose;
        }

        public int GetLostHealthValue(ActionResult result)
        {
            return result switch
            {
                ActionResult.LOSE => Randomizer.GetRandomValue(0, _maxLostHealthIfLose + 1),
                ActionResult.WIN => Randomizer.GetRandomValue(0, _maxLostHealthIfWon + 1),
                _ => throw new ArgumentException("Cannot find value for action result.")
            };
        }
    }
}