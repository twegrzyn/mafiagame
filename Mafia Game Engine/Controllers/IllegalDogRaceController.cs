﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using MafiaGameEngine.Helpers;
using MafiaGameEngine.Models.Player;

namespace MafiaGameEngine.Controllers
{
    public class IllegalDogRaceController
    {
        private readonly PlayerModel _player;
        private List<Dog> _dogs = new List<Dog>();

        public IllegalDogRaceController(PlayerModel player)
        {
            _player = player;
        }

        public void GoToRace()
        {
            GenerateDogsForRace();
            PrintDogs();
            try
            {
                Console.WriteLine("Pick dog: ");
                var dogPicked = int.Parse(Console.ReadLine());

                Console.WriteLine("How much do you want to bet? ");
                var betAmount = int.Parse(Console.ReadLine());
                if (_player.getMoneyModel().GetMoneyValue() < betAmount)
                {
                    Console.WriteLine("You don't have enough money.");
                    return;
                }
                if(betAmount <= 0)
                {
                    Console.WriteLine("Are you kidding me?");
                    return;
                }

                if (CheckWin(dogPicked))
                {
                    var winAmount = (decimal) (betAmount * (CalculateWinRatio(dogPicked) - 1));
                    _player.getMoneyModel().AddMoney(winAmount);
                    Console.WriteLine("You win " + winAmount.ToString("#,##0.00 $"));
                    
                }
                else
                {
                    _player.getMoneyModel().SubstractMoney(betAmount);
                }
                Thread.Sleep(2000);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Invalid action / format.");
            }
        }

        private bool CheckWin(int dogPicked)
        {
            var randomNumber = Randomizer.GetRandomValue(1, 101);
            var winDog = 0;
            for (var i = 0; i < _dogs.Capacity - 1; i++)
            {
                if (randomNumber <= _dogs[i].GetWinChance())
                {
                    winDog = i;
                    break;
                }

                randomNumber -= _dogs[i].GetWinChance();
            }

            Console.WriteLine("Win dog " + _dogs[winDog].GetName());
            if (winDog == dogPicked - 1)
            {
                Console.WriteLine("Congratulations! You win!");
                return true;
            }
            else
            {
                Console.WriteLine("Sorry! You lose this time!");
                return false;
            }
        }

        private void PrintDogs()
        {
            var i = 1;
            foreach (var dog in _dogs)
            {
                Console.WriteLine(i + ". " + dog.GetName());
                Console.WriteLine("Odds: 1:" + CalculateWinRatio(i));
                Console.WriteLine();
                i++;
            }
            Console.WriteLine();
        }

        private double CalculateWinRatio(int dogPicked)
        {
            return Math.Round((Math.Round((100d / _dogs[dogPicked - 1].GetWinChance()), 2) - 0.25), 2);
        }

        private void GenerateDogsForRace()
        {
            var dogsList = new List<Dog>();
            int firstDogChance = 0,
                secondDogChance = 0,
                thirdDogChance = 0,
                fourthDogChance = 0,
                fifthDogChance = 0;
            var regenerate = true;
            while (regenerate)
            {
                //generate first dog
                firstDogChance = Randomizer.GetRandomValue(12, 33);

                //generate second dog
                secondDogChance = Randomizer.GetRandomValue(12, 31);

                //generate third dog
                // if first two dog are strong (chance higher than 40%) then we have to create weaker dogs now
                var maxThirdDogChance = (firstDogChance + secondDogChance > 40) ? 20 : 31;
                thirdDogChance = Randomizer.GetRandomValue(12, maxThirdDogChance);

                var maxFourthDogChance = (firstDogChance + secondDogChance + thirdDogChance > 60) ? 20 : 31;
                fourthDogChance = Randomizer.GetRandomValue(12, maxFourthDogChance);

                fifthDogChance = 100 - (firstDogChance + secondDogChance + thirdDogChance + fourthDogChance);
                if (fifthDogChance < 12 || fifthDogChance > 35)
                {
                    regenerate = true;
                    continue;

                }
                regenerate = false;

            }

            dogsList.Add(new Dog(firstDogChance));
            dogsList.Add(new Dog(secondDogChance));
            dogsList.Add(new Dog(thirdDogChance));
            dogsList.Add(new Dog(fourthDogChance));
            dogsList.Add(new Dog(fifthDogChance));
            _dogs = new List<Dog>();
            _dogs.AddRange(ShuffleDogsList(dogsList));
        }

        private List<Dog> ShuffleDogsList(List<Dog> dogsList)
        {
            var rnd = new Random();
            return new List<Dog>(dogsList.OrderBy(item => rnd.Next()));
        }

        internal class Dog
        {
            private readonly string _name;
            public Dog(int chanceToWin)
            {
                _chanceToWin = chanceToWin;
                _name = Randomizer.GetRandomDogName();
            }

            private readonly int _chanceToWin; // 0 - 100

            public int GetWinChance() { return _chanceToWin; }
            public string GetName() { return _name; }
        }
    }
}
