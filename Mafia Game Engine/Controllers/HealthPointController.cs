﻿using MafiaGameEngine.Models.Player;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace MafiaGameEngine.Controllers
{
    public class HealthPointController
    {
        private readonly HealthPointModel _model;
        private readonly ILogger _logger;

        public HealthPointController(IConfiguration config, PlayerModel model, ILogger<HealthPointController> logger)
        {
            _model = model.getHealthPointModel();
            _logger = logger;
            var startHp = int.Parse(config.GetSection("Health").GetSection("HealthMaxValue").Value);
            _model.SetMaxValueOfHealthPoints(int.Parse(config.GetSection("Health").GetSection("HealthMaxValue").Value));
        }

        internal int HealthPoints => _model.GetHealthPoints();

        internal void ReduceHeathPoints(int value)
        {
            if (_model.GetHealthPoints() <= value)
            {
                _logger.Log(LogLevel.Information,"Player died!");
                //TODO EVENT
                return;
            }

            _model.ReduceHeathPoints(value);
        }

        internal int MaxHealthPoints => _model.GetMaxHealthPoints();

        internal void AddHealthPoints(int value)
        {
            if (_model.GetMaxHealthPoints() <= _model.GetHealthPoints() + value)
                _model.AddHealthPoints(value);
            else
                _model.SetHealthPointToMaxValue();
        }
    }
}