﻿using MafiaGameEngine.Models.Player;
using Microsoft.Extensions.Configuration;
using System;

namespace MafiaGameEngine.Controllers
{
    public class ExperienceController
    {
        private readonly ExperienceModel _model;
        // EXCEL (POWER((C7*C7*C7);1,08)*2)+15
        private readonly int _firstLevelExperienceRequired;

        public ExperienceController(PlayerModel model, IConfiguration config)
        {
            _model = model.getExperienceModel();
            _firstLevelExperienceRequired = int.Parse(config.GetSection("FirstLevelExperienceRequired").Value);
        }

        internal void AddExperience(long value)
        {
            _model.AddExperience(value);
            if (_model.GetExperiencePoints() >= CalculateRequiredLevelExperience(GetLevel())) _model.LevelUp();
        }

        internal int GetLevel()
        {
            return _model.GetLevel();
        }

        internal string Experience => _model.ToString();

        private long CalculateRequiredLevelExperience(int level)
        {
            return (long)Math.Round((Math.Pow(Math.Pow(level, 3d), 1.08d) * 2) + _firstLevelExperienceRequired);
        }

        private long CalculateExpToNextLevel()
        {
            return CalculateRequiredLevelExperience(GetLevel()) - _model.GetExperiencePoints();
        }
    }
}