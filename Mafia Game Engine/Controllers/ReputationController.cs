﻿using MafiaGameEngine.Models.Player;
using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;
using System.Threading;

namespace MafiaGameEngine.Controllers
{
    public class ReputationController
    {
        private readonly ReputationModel _model;
        private readonly IConfiguration config;
        private readonly int _reputationRequiredToWin;
        public event EventHandler EventProducer;

        public ReputationController(IConfiguration config, PlayerModel model)
        {
            this._model = model.getReputationModel();
            this.config = config;
            this._reputationRequiredToWin = int.Parse(config.GetSection("Reputation").GetSection("ToWinFirstPart").Value);
        }

        internal void AddReputation(long value)
        {
            _model.AddReputation(value);
            if(_model.GetValue() >= _reputationRequiredToWin)
            {
                EventProducer.Invoke(this, EventArgs.Empty);
            }
        }

        internal string Reputation => _model.ToString();

    }
}