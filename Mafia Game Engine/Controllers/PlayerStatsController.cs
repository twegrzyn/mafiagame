﻿using MafiaGameEngine.Models.Player;

namespace MafiaGameEngine.Controllers
{
    public class PlayerStatsController
    {
        private readonly PlayerModel _player;
        public PlayerStatsController(PlayerModel player)
        {
            _player = player;
        }

        public long CalculateMeleeFightPower()
        {
            return _player.getSkillsModel().Strength + _player.getExperienceModel().GetLevel();
        }
    }
}
