﻿using MafiaGameEngine.Models;
using MafiaGameEngine.Models.Player;

namespace MafiaGameEngine.Controllers
{
    public class MoneyController
    {
        private readonly MoneyModel _model;

        public MoneyController(PlayerModel model)
        {
            _model = model.getMoneyModel();
        }

        internal string Money => _model.ToString();


        internal void AddMoney(decimal money)
        {
            _model.AddMoney(money);
        }

        internal decimal GetMoneyValue()
        {
            return _model.GetMoneyValue();
        }

        internal string GetMoneyString()
        {
            return _model.ToString();
        }

    }
}