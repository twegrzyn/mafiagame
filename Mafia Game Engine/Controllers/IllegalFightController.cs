﻿using System;
using MafiaGameEngine.Helpers;
using MafiaGameEngine.Models.Player;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace MafiaGameEngine.Controllers
{
    public class IllegalFightController
    {
        private readonly PlayerModel _player;
        private readonly PlayerStatsController _playerStats;
        private readonly ILogger _logger;

        private static int BaseWinChance;
        private readonly int _LoseHpIfWonFrom;
        private readonly int _LoseHpIfWonTo;
        private readonly int _LoseHpIfLoseFrom;
        private readonly int _LoseHpIfLoseTo;
        private readonly int _MoneyIfWon;
        private readonly int _MoneyIfLose;

        public IllegalFightController(IConfiguration config, PlayerModel player, PlayerStatsController playerStats, ILogger<IllegalFightController> logger)
        {
            _player = player;
            _playerStats = playerStats;
            _logger = logger;
            BaseWinChance = int.Parse(config.GetSection("IllegalFight").GetSection("BaseWinChance").Value);
            _LoseHpIfWonFrom = int.Parse(config.GetSection("IllegalFight").GetSection("LoseHpIfWonFrom").Value);
            _LoseHpIfWonTo = int.Parse(config.GetSection("IllegalFight").GetSection("LoseHpIfWonTo").Value);
            _LoseHpIfLoseFrom = int.Parse(config.GetSection("IllegalFight").GetSection("LoseHpIfLoseFrom").Value);
            _LoseHpIfLoseTo = int.Parse(config.GetSection("IllegalFight").GetSection("LoseHpIfLoseTo").Value);
            _MoneyIfWon = int.Parse(config.GetSection("IllegalFight").GetSection("MoneyIfWon").Value);
            _MoneyIfLose = int.Parse(config.GetSection("IllegalFight").GetSection("MoneyIfLose").Value);

        }
        
        private void FightIllegal()
        {
            var winChance = (int)(BaseWinChance + Math.Ceiling((decimal)_playerStats.CalculateMeleeFightPower() / 10));
            _logger.LogInformation("Your win chance: {0}%", winChance);
            var enemyFightPower = Randomizer.GetRandomValue(1, 101);
            if (winChance > enemyFightPower)
            {
                _player.getHealthPointModel().ReduceHeathPoints(Randomizer.GetRandomValue(_LoseHpIfWonFrom, _LoseHpIfWonTo)); // wymyslić ile traci sie zdrowia
                _player.getMoneyModel().AddMoney(_MoneyIfWon); // wymyslić ile kasy za walke
                _logger.LogInformation("Your win! Earn {0} money", _MoneyIfWon);
            }
            else
            {
                _player.getHealthPointModel().ReduceHeathPoints(Randomizer.GetRandomValue(_LoseHpIfLoseFrom, _LoseHpIfLoseTo));
                _player.getMoneyModel().AddMoney(_MoneyIfLose);
                _logger.LogInformation("Your lose! Earn {0} money",-_MoneyIfLose);
            }
        }

        public void GoToIllegalFight()
        {
           FightIllegal();
        }
    }
}
