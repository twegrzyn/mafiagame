﻿using System;
using MafiaGameEngine.Models.Player;
using MafiaGameEngine.Persistence.Repositories;
using Microsoft.Extensions.Logging;

namespace MafiaGameEngine.Controllers
{
    public class PlayerController
    {
        private readonly PlayerModel _player;
        private readonly PlayerRepository _playerRepository;
        public ILogger Logger;

        public PlayerController(ILogger<PlayerController> logger, PlayerModel player, PlayerRepository playerRepository)
        {
            _player = player;
            Logger = logger;
            _playerRepository = playerRepository;
        }

        public void PrintPlayerDetails()
        {
            Console.WriteLine(_player.ToString());
        }

        public void SavePlayer()
        {
            _playerRepository.SavePlayer(_player);
        }
    }
}