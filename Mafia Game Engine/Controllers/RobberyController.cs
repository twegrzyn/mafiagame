﻿using System;
using System.Collections.Generic;
using System.Threading;
using MafiaGameEngine.Helpers;
using MafiaGameEngine.Models;
using MafiaGameEngine.Persistence.Repositories;
using Microsoft.Extensions.Logging;

namespace MafiaGameEngine.Controllers
{
    public class RobberyController
    {
        private readonly ILogger _logger;
        private readonly ExperienceController _experienceController;
        private readonly HealthPointController _healthPointController;
        private readonly ReputationController _reputationController;
        private readonly MoneyController _moneyController;
        private readonly RobberyRepository _robberyRepository;

        public RobberyController(ILogger<RobberyController> logger, ExperienceController exp,
            HealthPointController hp, ReputationController rep, MoneyController money, RobberyRepository robberyRepository)
        {
            _experienceController = exp;
            _healthPointController = hp;
            _reputationController = rep;
            _moneyController = money;
            _robberyRepository = robberyRepository;
            _logger = logger;
        }

        public Dictionary<RobberyTypeEnum, RobberyModel> GetAllRobberies()
        {
            return _robberyRepository.GetAllRobberies();
        }
        public void PrintRobberies()
        {
            var robberies = GetAllRobberies();
            var i = 1;
            Console.Clear();
            foreach (KeyValuePair<RobberyTypeEnum, RobberyModel> robberyKVObject in robberies)
            {
                Console.WriteLine("");
                Console.WriteLine(i + ". " + robberyKVObject.Value.ToString());
                Console.WriteLine("________________________");
                Console.WriteLine(robberyKVObject.Value.Description);
                Console.WriteLine("");
                Console.WriteLine("Possible reputation to earn: " + robberyKVObject.Value.Reputation);
                Console.WriteLine("Possible money to earn: " + robberyKVObject.Value.Money);
                Console.WriteLine("________________________");
                Console.WriteLine("************************");
                i++;
            }
            Console.WriteLine("________________________");

        }

        public void DoRobbery(RobberyTypeEnum type)
        {
            var robberyObject = _robberyRepository.GetRobberyObject(type);
            if (CheckWin(robberyObject))
            {
                Console.WriteLine("Your robbery was successful.");
                _moneyController.AddMoney(robberyObject.Money.GetEarnedMoney());
                _healthPointController.ReduceHeathPoints(robberyObject.LostHealth.GetLostHealthValue(ActionResult.WIN));
                _experienceController.AddExperience(robberyObject.Experience);
                _reputationController.AddReputation(robberyObject.Reputation.GetEarnReputation());
            }
            else
            {
                Console.WriteLine("Your robbery has failed.");
                _healthPointController.ReduceHeathPoints(robberyObject.LostHealth.GetLostHealthValue(ActionResult.LOSE));
            }
            Console.WriteLine("Your money: {0} ", _moneyController.GetMoneyString());
            Thread.Sleep(2000);
            _logger.LogDebug("Player money: " + _moneyController.GetMoneyValue());
        }

        private bool CheckWin(RobberyModel robbery)
        {
            var chance = CalculateWinChance(robbery);
            return CheckWinBasedOnChance(chance);
        }

        private bool CheckWinBasedOnChance(int chance)
        {
            if (chance >= 100)
                return true;
            return !(Randomizer.GetRandomValue(0, 100) > chance);
        }

        private int CalculateWinChance(RobberyModel robbery)
        {
            var chance = 0;
            //TODO complicated calculation with player skill and player abilities and level etc.
            // should properly calculate chance for win and lose and check it

            // Currently -> simple implementation

            var baseChanceToWin = robbery.SuccessChance;
            var chanceAfterLevelCalculated = (int)(baseChanceToWin + Math.Floor(_experienceController.GetLevel() / 10m));
            chance = chanceAfterLevelCalculated;

            return chance;
        }
    }
}