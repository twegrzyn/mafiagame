﻿using System;
using System.Collections.Generic;

namespace MafiaGameEngine.Persistence.Repositories
{
    public static class DogsNameRepository
    {
        private static readonly List<string> DogsName = new List<string>();

        private static readonly string DogsNamePath = Environment.CurrentDirectory + "\\Resources\\dogs.txt";

        private static void LoadDogsName()
        {
            var names = System.IO.File.ReadAllLines(DogsNamePath);
            DogsName.AddRange(names);
        }

        public static List<string> GetDogsName()
        {
            if(DogsName == null || DogsName.Capacity == 0)
                LoadDogsName();
            return DogsName;
        }
    }
}
