﻿using System.Collections.Generic;
using MafiaGameEngine.Models;

namespace MafiaGameEngine.Persistence.Repositories
{
    public enum RobberyTypeEnum
    {
        OnTheStreet = 1,
        Truck = 2,
        Taxi = 3,
        Pub = 4,
        Bank = 5
    }

    public class RobberyRepository
    {
        private static Dictionary<RobberyTypeEnum, RobberyModel> robberyTypeDictionary =
            new Dictionary<RobberyTypeEnum, RobberyModel>
            {
                {
                    RobberyTypeEnum.OnTheStreet, new RobberyModel
                    {
                        Name = "Pickpocketing",
                        Description = "Try to rob someone on the street. Good chance of success.",
                        Money = new EarnMoneyModel(0.03M, 0.12M),
                        Reputation = new EarnReputationModel(1, 5),
                        SuccessChance = 96,
                        SuperEarnChance = 3,
                        Experience = 3,
                        LostHealth = new LostHealthModel(1, 3)
                    }
                },
                {
                    RobberyTypeEnum.Taxi, new RobberyModel
                    {
                        Name = "Taxi robbery",
                        Description = "Get in a taxi and get as much as you can. Good chance of success",
                        Money = new EarnMoneyModel(0.09M, 0.28M),
                        Reputation = new EarnReputationModel(3, 7),
                        SuccessChance = 75,
                        SuperEarnChance = 2,
                        Experience = 5,
                        LostHealth = new LostHealthModel(2, 5)
                    }
                },
                {
                    RobberyTypeEnum.Truck, new RobberyModel
                    {
                        Name = "Convoy robbery",
                        Description = "Raid a convoy of trucks. Average chance of success.",
                        Money = new EarnMoneyModel(0.12M, 0.88M),
                        Reputation = new EarnReputationModel(5, 8),
                        SuccessChance = 55,
                        SuperEarnChance = 3,
                        Experience = 8,
                        LostHealth = new LostHealthModel(2, 12)
                    }
                },
                {
                    RobberyTypeEnum.Pub, new RobberyModel
                    {
                        Name = "Pub robbery",
                        Description = "Raid a pub like in westerns. Average chance of success.",
                        Money = new EarnMoneyModel(1.00M, 5.88M),
                        Reputation = new EarnReputationModel(12, 25),
                        SuccessChance = 12,
                        SuperEarnChance = 2,
                        Experience = 25,
                        LostHealth = new LostHealthModel(7, 20)
                    }
                },
                {
                    RobberyTypeEnum.Bank, new RobberyModel
                    {
                        Name = "Bank robbery",
                        Description = "",
                        Money = new EarnMoneyModel(50.00M, 300.00M),
                        Reputation = new EarnReputationModel(50, 75),
                        SuccessChance = 8,
                        SuperEarnChance = 2,
                        Experience = 150,
                        LostHealth = new LostHealthModel(10, 60)
                    }
                }
            };


        public Dictionary<RobberyTypeEnum, RobberyModel> GetAllRobberies()
        {
            return robberyTypeDictionary;
        }

        public RobberyModel GetRobberyObject(RobberyTypeEnum type)
        {
            return robberyTypeDictionary.GetValueOrDefault(type);
        }
    }
}