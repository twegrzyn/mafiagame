﻿using MafiaGameEngine.Models.Player;
using MafiaGameEngine.Persistence.SaveLoadGame;

namespace MafiaGameEngine.Persistence.Repositories
{
    public class PlayerRepository
    {
        private readonly ISaveLoadGameSerivce _saveLoadGame;
        public PlayerRepository(ISaveLoadGameSerivce saveLoadGame)
        {
            _saveLoadGame = saveLoadGame;
        }
        public PlayerModel LoadPlayer()
        {
            return _saveLoadGame.LoadGame();
        }

        public void SavePlayer(PlayerModel player)
        {
            _saveLoadGame.SaveGame(player);
        }
    }
}
