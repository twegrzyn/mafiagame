﻿using MafiaGameEngine.Helpers;
using MafiaGameEngine.Models.Player;
using Newtonsoft.Json;
using System;
using System.IO;


namespace MafiaGameEngine.Persistence.SaveLoadGame
{
    internal class SaveLoadGameFromEncryptedFile : ISaveLoadGameSerivce
    {

        private static readonly string SavePath = Environment.CurrentDirectory + "\\player_DO_NOT_TOUCH.encrypted";

        public PlayerModel LoadGame()
        {
            if (!File.Exists(SavePath))
            {
                SaveLoadDataFromEncryptedFile.SaveEncryptedData(GetPlayerJson(
                    new PlayerModel(
                        new ExperienceModel(),
                        new HealthPointModel(),
                        new ReputationModel(),
                            new MoneyModel(),
                        new SkillsModel())), SavePath);
            }
            return LoadData();
        }

        public void SaveGame(PlayerModel player)
        {
            SaveLoadDataFromEncryptedFile.SaveEncryptedData(GetPlayerJson(player), SavePath);
        }

        public void RemoveSaveFile()
        {
            if (File.Exists(SavePath))
                File.Delete(SavePath);
        }

        private static string GetPlayerJson(PlayerModel player)
        {
            var settings = new JsonSerializerSettings { ContractResolver = new PlayerModelResolver() };
            return JsonConvert.SerializeObject(player, Formatting.Indented, settings);
        }

        private static PlayerModel LoadData()
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new PlayerModelResolver(),
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor
            };
            var text = SaveLoadDataFromEncryptedFile.DecryptFile(SavePath);
            return JsonConvert.DeserializeObject<PlayerModel>(text, settings);
        }


    }
}
