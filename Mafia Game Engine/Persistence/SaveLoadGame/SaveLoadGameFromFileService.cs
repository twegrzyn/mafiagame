﻿using System;
using System.IO;
using MafiaGameEngine.Models.Player;
using Newtonsoft.Json;

namespace MafiaGameEngine.Persistence.SaveLoadGame
{
    internal class SaveLoadGameFromFileService : ISaveLoadGameSerivce
    {
        private static readonly string SavePath = Environment.CurrentDirectory + "\\player.json";

        public PlayerModel LoadGame()
        {
            if (!File.Exists(SavePath))
            {
                File.WriteAllText(SavePath,
                    GetPlayerJson(
                        new PlayerModel(
                            new ExperienceModel(),
                            new HealthPointModel(),
                            new ReputationModel(),
                                new MoneyModel(),
                            new SkillsModel())));
            }
            return LoadData();
        }

        public void SaveGame(PlayerModel player)
        {
            File.WriteAllText(SavePath, GetPlayerJson(player));
        }

        public void RemoveSaveFile()
        {
            if (File.Exists(SavePath))
                File.Delete(SavePath);
        }

        private static string GetPlayerJson(PlayerModel player)
        {
            var settings = new JsonSerializerSettings { ContractResolver = new PlayerModelResolver() };
            return JsonConvert.SerializeObject(player, Formatting.Indented, settings);
        }

        private static PlayerModel LoadData()
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new PlayerModelResolver(),
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor
            };
            var text = File.ReadAllText(SavePath);
            return JsonConvert.DeserializeObject<PlayerModel>(text, settings);
        }
    }
}
