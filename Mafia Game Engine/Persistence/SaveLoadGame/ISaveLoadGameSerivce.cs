﻿using MafiaGameEngine.Models.Player;

namespace MafiaGameEngine.Persistence.SaveLoadGame
{
    public interface ISaveLoadGameSerivce
    {
        public PlayerModel LoadGame();
        public void SaveGame(PlayerModel player);

        public void RemoveSaveFile();

    }
}
